//nicht entfernen
#include <Robot.h>
#include <SPI.h>
#include <RFCom.h>

RFCom RF;

void setup() {
    //nicht entfernen:
    initAll();
	  RF.init();
    //hier kann weiterer Code folgen der einmal ausgefuehrt werden soll:
    RF.pair(11);
}

void loop() { //Code hierdrinnen wird endlos wiederholt
     switch(getBtn(4))
     {
      case 0:
        RF.moto(0,0);
        break;
      case 1:
        RF.moto(255,0);
        break;
      case 2:
        RF.moto(0,255);
        break;
      case 3:
        RF.moto(255,255);
        break;
      case 4:
        RF.moto(-255,-255);
        break;
     }
}