//nicht entfernen
#include <Robot.h>
#include <SPI.h>
#include <RFCom.h>

#define SLAVE   11

RFCom RF;

void setup() {
    //nicht entfernen:
    initAll();
    RF.init();
    //hier kann weiterer Code folgen der einmal ausgefuehrt werden soll:
    RF.pair(SLAVE);   //hier wird der Roboter mit dem anderem verbunden
    
}

void loop() { //Code hierdrinnen wird endlos wiederholt
   RF.ledOn();
   ledOn();
   delay(500);
   RF.ledOff();
   ledOff();
   delay(500);
}