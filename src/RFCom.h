// RFCom.h

#ifndef _RFCOM_h
#define _RFCOM_h

#include "RF24.h"
#include "Robot.h"

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define ADDR_PRE 0xAAAA00
#define MAGIC_CMD 0xbb
#define MAGIC_DAT 0xcc
#define NO_MSG	0xFFFFFF

#define CMD_LED		0x10
#define CMD_MOTL	0x11
#define CMD_MOTR	0x12

class RFCom
{
 protected:
	 void handle_cmd(uint16_t*);

 public:
	RFCom();
	void pair(uint8_t addr);
	void write(long data);
	void writeChar(char data);
	long read();
	char readChar();
	bool available();
	void act();
	void ledOn();
	void ledOff();
	void motoR(int16_t speed);
	void motoL(int16_t speed);
	void moto(int16_t left, int16_t right);
	void init();
	
	

};

#endif

//extern RFCom RF;
