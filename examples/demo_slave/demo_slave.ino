//nicht entfernen
#include <Robot.h>
#include <SPI.h>
#include <RFCom.h>

#define MASTER   4

RFCom RF;

void setup() {
    //nicht entfernen:
    initAll();
	RF.init();
    //hier kann weiterer Code folgen der einmal ausgefuehrt werden soll:
    RF.pair(MASTER);   //hier wird der Roboter mit dem anderem verbunden
    
}

void loop() { //Code hierdrinnen wird endlos wiederholt
     RF.act();
}
