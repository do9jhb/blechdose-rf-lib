// 
// 
// 

#include "RFCom.h"

RF24 radio(CE,CS);
uint8_t myAddr;
//uint8_t remote_addr[5];

union data2long
{
	long l;
	uint16_t c[6];
};

union long2char
{
	long l;
	char c;
};


RFCom::RFCom()
{
	
}

void RFCom::init()
{
	radio.begin();
	radio.setPALevel(RF24_PA_LOW);		//too much power can cause issues on near distances
	radio.setAddressWidth(3);
	radio.setPayloadSize(12);
	radio.setDataRate(RF24_1MBPS);
	myAddr = getSN();
}

void RFCom::pair(uint8_t addr)
{
	//radio.printDetails();
	radio.openWritingPipe(ADDR_PRE | addr);
	radio.openReadingPipe(1, ADDR_PRE | myAddr);
	radio.startListening();
}


void RFCom::write(long data)
{
	radio.stopListening();
	data2long d;
	d.l=data;
	d.c[0] = MAGIC_DAT;
	radio.write(d.c, sizeof(d.c));
	radio.startListening();
}

void RFCom::writeChar(char data)
{
	long2char lc;
	lc.c = data;
	write(lc.l);
}

long RFCom::read()
{

	if (radio.available())
	{
		data2long d;
		radio.read(d.c, sizeof(d.c));
		if (d.c[0] == MAGIC_CMD)
		{
			handle_cmd(d.c);
			return NO_MSG;
		}
		else
		{
			return d.l;
		}
	}
	return NO_MSG;
}

char RFCom::readChar()
{
	long2char lc;
	lc.l = read();
	return lc.c;
}

bool RFCom::available()
{
	return radio.available();
}


void RFCom::act()
{
	if (radio.available())
	{
		data2long d;
		radio.read(d.c, sizeof(d));
		/*
		Serial.println("Payload:");
		Serial.println(d.c[0], 16);
		Serial.println(d.c[1], 16);
		Serial.println(d.c[2], 16);
		Serial.println(d.c[3], 16);
		Serial.println(d.c[4], 16);
		Serial.println(d.c[5], 16);
		Serial.println();
		*/
		if (d.c[0] == MAGIC_CMD)
		{
			handle_cmd(d.c);
		}
	}
}



void RFCom::handle_cmd(uint16_t* data)
{
	switch (data[1])
	{
	case CMD_LED:
		ledSet(data[2]);
		break;
	case CMD_MOTL:
	{
		//Serial.println(data[2]);
		//Serial.println();
		::motoL(data[2]);
		break;
	}
	case CMD_MOTR:
	{
		//Serial.println(data[2]);
		//Serial.println();
		::motoR(data[2]);
		break;
	}
	}
}

void RFCom::ledOn()
{
	radio.stopListening();
	int d[6] = { MAGIC_CMD, CMD_LED, HIGH, 0, 0, 0 };
	radio.write(&d, sizeof(d));
	radio.startListening();
}

void RFCom::ledOff()
{
	radio.stopListening();
	int d[6] = { MAGIC_CMD, CMD_LED, LOW, 0, 0, 0 };
	radio.write(&d, sizeof(d));
	radio.startListening();
}

void RFCom::motoR(int16_t speed)
{
	radio.stopListening();
	int d[6] = { MAGIC_CMD, CMD_MOTR, speed, 0, 0, 0};
	radio.write(&d, sizeof(d));
	radio.startListening();
}

void RFCom::motoL(int16_t speed)
{
	radio.stopListening();
	int d[6] = { MAGIC_CMD, CMD_MOTL, speed, 0, 0, 0 };
	radio.write(&d, sizeof(d));
	radio.startListening();
}

void RFCom::moto(int16_t left, int16_t right)
{
	this->motoL(left);
	this->motoR(right);
}

//RFCom RF = RFCom();
